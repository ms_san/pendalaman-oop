<?php 

trait Hewan
{
  public $nama;
  public $darah = 50;
  public $jumlahKaki;
  public $keahlian;

  public function atraksi() 
  {
    echo $this->nama . " sedang " . $this->keahlian . "<br>";
  }
}

trait Fight
{
  public $attackPower;
  public $defencePower;

  public function serang($lawan) 
  {
    echo $this->nama . " sedang menyerang " . $lawan->nama . "<br>";
  }
  public function diSerang($lawan) 
  {
    echo $lawan->nama . " Sedang diserang " . $this->nama . "<br>";
    $darahsisa = $lawan->darah - ($this->attackPower/$lawan->defencePower);
    echo "Darah " . $lawan->nama . " tersisa ";
    return $darahsisa;
  }
}

class Elang
{
  use Hewan, Fight;
  
  function __construct($nama) {
    $this->nama = $nama;
    $this->darah;
    $this->jumlahKaki = 2;
    $this->keahlian = "terbang Tinggi";
    $this->attackPower = 10;
    $this->defencePower = 5;
  }

  public function getInfoHewan(){
    $all = "Nama hewan " . $this->nama . " Darah " . $this->darah . " Keahlian " . $this->keahlian . " Attack Power " . $this->attackPower . " Defence Power " . $this->defencePower;
    return $all;
  } 
  
}

class Harimau
{
  use Hewan, Fight;
  
  function __construct($nama) {
    $this->nama = $nama;
    $this->darah;
    $this->jumlahKaki = 4;
    $this->keahlian = "Lari cepat";
    $this->attackPower = 7;
    $this->defencePower = 8;
  }

  public function getInfoHewan(){
    $all = "Nama hewan " . $this->nama . " Darah " . $this->darah . " Keahlian " . $this->keahlian . " Attack Power " . $this->attackPower . " Defence Power " . $this->defencePower;
    return $all;
  } 
  
}
$elang = new Elang("Elang sawah");
echo $elang->atraksi();

$harimau = new Harimau("Harimau jawa");
echo $harimau->atraksi();

$elang = new Elang("Elang sawah");
echo "<br>";
echo $elang->serang($harimau);
echo $elang->diSerang($harimau);
echo "<br>";

$info = new Elang("Elang sawah");
echo "<br>";
echo $info->getInfoHewan($elang);
?>